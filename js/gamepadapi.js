const gamepadapi = {
	buttonMap : {0:"B",1:"A",8:"SELECT",9:"START",12:"UP",13:"DOWN",14:"LEFT",15:"RIGHT"},
	addGamepad(gamepad){
		console.log('add:', gamepad.index);
		players.push(new Players(gamepad));
	},
	removeGamepad(gamepad) {
		if (players[gamepad.index]) {
			delete players[gamepad.index];
		}
	},
	addGamepadIfNew(gamepad) {
		if (!players[gamepad.index]) {
			gamepadapi.addGamepad(gamepad);
		} else {
			players[gamepad.index].gamepad = gamepad;
		}
	},
	handleConnect(e) {
		console.log('connect');
		gamepadapi.addGamepadIfNew(e.gamepad);
	},
	handleDisconnect(e) {
		console.log('disconnect');
		gamepadapi.removeGamepad(e.gamepad);
	},
	processController(info) {
		gamepadapi.buttonRouter(info);
	},
	addNewPads() {
		const gamepads = navigator.getGamepads();
		for (let i = 0; i < gamepads.length; i++) {
			const gamepad = gamepads[i]
			if (gamepad) {
				gamepadapi.addGamepadIfNew(gamepad);
			}
		}
	},
	process() {
		gamepadapi.addNewPads();  // some browsers add by polling, others by event
		Object.values(players).forEach(gamepadapi.processController);
		requestAnimationFrame(gamepadapi.process);
	},
	buttonRouter(info){
		players[info.gamepad.index].buttonRouter(info);
	}
}
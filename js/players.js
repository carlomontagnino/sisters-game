var players = [];

function Players(gamepad){
	this.gamepad = gamepad;
	this.index = gamepad.index;
	this.player_number = gamepad.index+1;
	this.player_label = 'Player '+(gamepad.index+1);
	this.status = {
		indoorway : false
	};
	this.sister = false;
	this.sprite = false;
	this.items = [];
	this.buttonCache = [];
}

// GETTERS
Players.prototype.getPlayerLabel = function(){
	return this.player_label;
}
Players.prototype.getPlayerNumber = function(){
	return this.player_number;
}
Players.prototype.getMRPlayerLabel = function(){
	return this.player_label.toLowerCase().replace(' ','-');
}
Players.prototype.getStatus = function(key){
	if(this.status[key]){
		return this.status[key];
	}
	return false;
}
Players.prototype.getSister = function(key){
	if(key){
		return this.sister[key];
	}else{
		return this.sister;
	}
	
}

// SETTERS
Players.prototype.setCharacterName = function(name){
	this.character.name = name;
}
Players.prototype.setStatus = function(key, value){
	this.status[key] = value;
}


Players.prototype.buttonRouter = function(info){
//	console.log('buttonRouter', this, info)
	var buttonsPressed = [];
	if(info.gamepad.buttons){
		for(var b=0;b<info.gamepad.buttons.length;b++){
			if(info.gamepad.buttons[b].pressed){
				buttonsPressed.push(gamepadapi.buttonMap[b]);
			}
		}
	}
	if(
		( // NO D-PAD
			(buttonsPressed.indexOf("UP") == -1) && 
			(buttonsPressed.indexOf("DOWN") == -1) && 
			(buttonsPressed.indexOf("LEFT") == -1) && 
			(buttonsPressed.indexOf("RIGHT") == -1)
		) || // OR
		( // GAME IS IN NON-PLAYING STATE
			game.inMenu || 
			game.inDialog || 
			game.isPaused
		) 
	){ 
		// prevent the polling of button pressed over the duration of a hold (no matter how short)
		if(!utils.areValuesTheSame(this.buttonCache,buttonsPressed)){
			this.doAction(buttonsPressed);
		}
	}else{
		this.doAction(buttonsPressed);
	}
	this.buttonCache = buttonsPressed;
}

Players.prototype.createSprite = function(){
	var spriteRaw = document.createElement('div');
		spriteRaw.id = this.getMRPlayerLabel();
		spriteRaw.classList.add('player');
		spriteRaw.dataset.indoorway = this.getStatus('indoorway');
		spriteRaw.dataset.playernumber = this.getPlayerNumber();
		spriteRaw.dataset.sister = this.getSister('name');
	game.sprites.appendChild(spriteRaw);
	this.sprite = document.getElementById(this.getMRPlayerLabel());
	this.isActive = true;
	if(sceneList[game.currentScene].playerStartingPoints){
		scene.moveChars(sceneList[game.currentScene].playerStartingPoints);
	}	
}

Players.prototype.doAction = function(buttons){
	if(buttons.length > 0){
		console.log('buttons pressed:', buttons)
	}
	if(buttons.indexOf("START") != -1){ 
		if(game.currentScene == 'intro'){
			scene.loadScene('characterSelect');
		}
		if(game.currentScene == 'characterSelect'){
			game.gameBegan = true;
			// LOOK AT THIS
			for(var p=0;p<players.length;p++){
				if(!players[p].sister){
					game.gameBegan = false;
				}
			}
			if(game.gameBegan){
				game.inMenu = false;
				this.createSprite();
			//	hud.createHud();
				scene.loadScene('outside');
			}
		}
	}
	if(buttons.indexOf("SELECT") != -1){ 
		
	}
	if(buttons.indexOf("UP") != -1){ 
		if(game.currentScene == 'characterSelect'){
			this.cycleSisterSelect('prev');
		}
		if(game.gameBegan){
			this.walk('UP');
		}
	}
	if(buttons.indexOf("DOWN") != -1){ 
		if(game.currentScene == 'characterSelect'){
			this.cycleSisterSelect('next');
		}
		if(game.gameBegan){
			this.walk('DOWN');
		}
	}
	if(buttons.indexOf("LEFT") != -1){ 
		if(game.inDialog){
			dialogBox.cycleDialogOptions('prev');
		}else if(game.gameBegan){
			this.walk('LEFT');
		}
	}
	if(buttons.indexOf("RIGHT") != -1){ 
		if(game.inDialog){
			dialogBox.cycleDialogOptions('next');
		}else if(game.gameBegan){
			this.walk('RIGHT');
		}
	}
	if(
		(buttons.indexOf("UP") == -1) &&
		(buttons.indexOf("DOWN") == -1) &&
		(buttons.indexOf("LEFT") == -1) &&
		(buttons.indexOf("RIGHT") == -1)
	){ 
		if(game.gameBegan){
			this.walk('STOP');
		}
	}
	if(buttons.indexOf("B") != -1){ 
		
	}
	if(buttons.indexOf("A") != -1){ 
		if(game.currentScene == 'characterSelect'){
			this.selectSister()
		}
		if(game.inDialog){
			if(dialogBox.hasOptions){
				dialogBox.selectOption();
			}else{
				dialogBox.closeDialogBox();
			}
		}else if(game.gameBegan){
			if(this.getStatus('indoorway')){
				this.checkDoor(this.status.indoorway);
			}
			if(this.getStatus('nearitem')){
				this.interactWithItem(this.status.nearitem);
			}
		}
	}

	// RESTART GAME
	if(
		(buttons.indexOf("SELECT") != -1) &&
		(buttons.indexOf("START") != -1)
	){ 
		var restart = confirm("Are you sure you want to restart the game?");
		if(restart){
			location.reload();
		}
	}
}

Players.prototype.applySister = function(sister){
	this.sister = sister;
}

Players.prototype.selectSister = function(){
	var characters = document.getElementsByClassName('character');
	for(var c=0;c<characters.length;c++){
		if(
			(characters[c].dataset.player == this.index+1) && 
			(characters[c].classList.contains('selected'))
		){
			characters[c].classList.remove('selected');
		}
	}
	for(var c=0;c<characters.length;c++){
		if(characters[c].dataset.player == this.index+1){
			characters[c].classList.add('selected');
			this.applySister(sisters[c]);
		}
	}
}

Players.prototype.cycleSisterSelect = function(dir){
	var characters = document.getElementsByClassName('character');
	var newKey,hoveredKey;
	var noHovered = true;
	for(var c=0;c<characters.length;c++){
		if(
			(characters[c].classList.contains('hovered')) &&
			(characters[c].dataset.player == this.index+1)
		){
			hoveredKey = c;
			noHovered = false;
			characters[c].classList.remove('hovered');
			characters[c].dataset.player = false;
		}
	}
	if(noHovered){
		newKey = 0;
	}else{
		if(dir == 'next'){
			if(characters[hoveredKey+1]){
				newKey = hoveredKey+1;
			}else{
				newKey = 0;
			}
		}
		if(dir == 'prev'){
			if(characters[hoveredKey-1]){
				newKey = hoveredKey-1;
			}else{
				newKey = characters.length-1
			}
		}
	}
	characters[newKey].classList.add('hovered');
	characters[newKey].dataset.player = this.index+1;
}

Players.prototype.setWalkingClasses = function(dir){
	dir = dir.toLowerCase();
	this.sprite.classList.remove('walking');
	this.sprite.classList.remove('walking-down');
	this.sprite.classList.remove('walking-left');
	this.sprite.classList.remove('walking-right');
	this.sprite.classList.remove('walking-up');
	if(dir != 'stop'){
		this.sprite.classList.add('walking');
		this.sprite.classList.add('walking-'+dir);
	}
}

Players.prototype.walk = function(dir){
	var y = this.sprite.offsetTop; 
	var x = this.sprite.offsetLeft; 
	var nextStep, newPos;
	this.setWalkingClasses(dir, this.sprite);
	this.setStatus('indoorway', this.isPlayerInDoor());
	if(this.getStatus('indoorway')){
		this.sprite.dataset.indoorway = true;
	}else{
		this.sprite.dataset.indoorway = false;
	}
	this.setStatus('nearitem', this.isPlayerNearItem());
	if(this.nearitem){
		this.sprite.dataset.nearitem = true;
	}else{
		this.sprite.dataset.nearitem = false;
	}
	switch(dir){
		case "UP":
			nextStep = y-game.step;
			if(nextStep <= this.defaultBounds.top){
				nextStep = this.defaultBounds.top;
			}
			if(this.willHitNoGoZone(dir)){
				nextStep = y;
			}
			this.sprite.style.top = nextStep+'px';
			break;
		case "DOWN":
			nextStep = y+game.step;
			if(nextStep >= this.defaultBounds.bottom){
				nextStep = this.defaultBounds.bottom;
			}
			if(this.willHitNoGoZone(dir)){
				nextStep = y;
			}
			this.sprite.style.top = nextStep+'px';
			break;
		case "LEFT":
			nextStep = x-game.step;
			if(nextStep <= this.defaultBounds.left){
				nextStep = this.defaultBounds.left;
			}
			if(this.willHitNoGoZone(dir)){
				nextStep = x;
			}
			this.sprite.style.left = nextStep+'px';
			break;
		case "RIGHT":
			nextStep = x+game.step;
			if(nextStep >= this.defaultBounds.right){
				nextStep = this.defaultBounds.right;
			}
			if(this.willHitNoGoZone(dir)){
				nextStep = x;
			}
			this.sprite.style.left = nextStep+'px';
			break;
		case "STOP" : 

			break;
	}
}

Players.prototype.isPlayerInDoor = function(){
	var spritePos = {
		top : this.sprite.offsetTop,
		bottom : this.sprite.offsetTop + this.sprite.offsetHeight,
		left : this.sprite.offsetLeft,
		right : this.sprite.offsetLeft + this.sprite.offsetWidth
	}
	var doors = game.world.getElementsByClassName('door');
	for(var d=0;d<doors.length;d++){
		var doorBox = {
			top : doors[d].offsetTop,
			bottom : doors[d].offsetTop + doors[d].offsetHeight,
			left : doors[d].offsetLeft,
			right : doors[d].offsetLeft + doors[d].offsetWidth
		}
		if(
			(spritePos.top <= (doorBox.top+5)) &&
			(spritePos.top >= (doorBox.top-5)) &&
			(spritePos.left >= doorBox.left-5) &&
			(spritePos.right <= doorBox.right+5)
		){
			//console.warn('IN DOOR '+d)
			return doors[d];
		}
	}
	return false;
}

Players.prototype.isPlayerNearItem = function(){
	var spritePos = {
		top : this.sprite.offsetTop,
		bottom : this.sprite.offsetTop + this.sprite.offsetHeight,
		left : this.sprite.offsetLeft,
		right : this.sprite.offsetLeft + this.sprite.offsetWidth
	}
	var items = game.world.getElementsByClassName('item-sprite');
	for(var i=0;i<items.length;i++){
		var itemBox = {
			top : items[i].offsetTop,
			bottom : items[i].offsetTop + items[i].offsetHeight,
			left : items[i].offsetLeft,
			right : items[i].offsetLeft + items[i].offsetWidth
		}
		if(
			(spritePos.top-48 <= itemBox.top) &&
			(spritePos.bottom+10 >= itemBox.top) &&
			(spritePos.left-10 <= itemBox.left) &&
			(spritePos.right+10 >= itemBox.right)
		){
			return items[i];
		}
	}
	return false;
}

Players.prototype.checkDoor = function(door){
	if(
		(door.dataset.doorstatus == 'open') ||
		(door.dataset.key && game.hasItem(this, door.dataset.key))
	){
		if(
			(door.dataset.gotopoint) ||
			(door.dataset.gotoscene)
		){
			this.goThroughDoor(door, this.sprite);
		}else{
			dialogBox.displayDialogBox(this.sister.name+": I'm not sure about this...", false);
		}	
	}else{
		dialogBox.displayDialogBox(this.sister.name+": I don't have the key for this door.", false);
	}
}

Players.prototype.goThroughDoor = function(door){
	console.log('goThroughDoor', door);
	if(door.dataset.gotopoint){
		var getPoints = door.dataset.gotopoint.split(',');
		var points = [
			[getPoints[0],getPoints[1]],
			[getPoints[2],getPoints[3]],
			[getPoints[4],getPoints[5]]
		];
		game.moveChars(points);
	}else if(door.dataset.gotoscene){
		startPoint = false;
		if(door.dataset.returnstartpoint){
			startPoint = door.dataset.returnstartpoint.split(',');
			var points = [
				[startPoint[0],startPoint[1]],
				[startPoint[2],startPoint[3]],
				[startPoint[4],startPoint[5]]
			];
		}
		scene.loadScene(door.dataset.gotoscene, points);
	}else{
//			dialogBox.displayDialogBox("I shouldn't be here.", false);
	}
}

Players.prototype.willHitNoGoZone = function(dir){
	var footline = {
		top : this.sprite.offsetTop+46,
		bottom : this.sprite.offsetTop + this.sprite.offsetHeight,
		left : this.sprite.offsetLeft,
		right : this.sprite.offsetLeft + this.sprite.offsetWidth
	}
	var noGoZones = world.getElementsByClassName('no-go-zone');
	if(noGoZones){
		for(var z=0;z<noGoZones.length;z++){
			var zoneBox = {
				top : noGoZones[z].offsetTop,
				bottom : noGoZones[z].offsetTop + noGoZones[z].offsetHeight,
				left : noGoZones[z].offsetLeft,
				right : noGoZones[z].offsetLeft + noGoZones[z].offsetWidth
			}
			switch(dir){
				case "UP":
					footline.top = footline.top-(game.step/game.collisionDivisor);
					break;
				case "DOWN":
					footline.bottom = footline.bottom+(game.step/game.collisionDivisor);
					break;
				case "LEFT":
					footline.left = footline.left-(game.step/game.collisionDivisor);
					break;
				case "RIGHT":
					footline.right = footline.right+(game.step/game.collisionDivisor);
					break;
			}
			if(
				(footline.top < zoneBox.bottom) &&
				(footline.bottom > zoneBox.top) &&
				(footline.left < zoneBox.right) &&
				(footline.right > zoneBox.left)
			){
				return true;
			}
		}
	}
	return false;
}

Players.prototype.interactWithItem = function(itemSprite){
	var item = sceneList[game.currentScene].bgMap.items[itemSprite.dataset.itemkey];
	console.log('interactWithItem', item);
	if(item.message){
		dialogBox.displayDialogBox(item.message, item.msgOptions);
	}else{
		// add to inventory
	}
}

Players.prototype.defaultBounds = {
	top : utils.spriteBuffer(0,'top'),
	right: utils.spriteBuffer(800,'right'), 
	bottom: utils.spriteBuffer(600,'bottom'), 
	left: utils.spriteBuffer(0,'left')
}





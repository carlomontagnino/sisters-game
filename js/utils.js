const utils = {
	areValuesTheSame(a, b) {
		return a.sort().join(' ') === b.sort().join(' ');
	},
	spriteBuffer(boundary, dir){
		switch(dir){
			case 'right':
				return boundary-32;
				break;
			case 'bottom':
				return boundary-48;
				break;
			default :
				return boundary;
		}
	}
}
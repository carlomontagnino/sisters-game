const scene = {
	loadScene(sceneID, startPoint){
		console.log('scene.loadScene('+sceneID+',['+startPoint+'])', sceneList[sceneID]);
		game.world.innerHTML = '';
		game.stage.dataset.scene = sceneID;
		game.currentScene = sceneID;
		if(sceneList[sceneID].html){
			game.world.innerHTML = sceneList[sceneID].html;
		}
		if(sceneList[sceneID].bgMap){
			bgImg = document.createElement('img');
			bgImg.src = sceneList[sceneID].bgMap.src;
			bgImg.classList.add('bgMap');
			bgImg.id = sceneID;
			bgImg.style.width = sceneList[sceneID].bgMap.width;
			world.appendChild(bgImg);
			if(sceneList[sceneID].bgMap.nogo){
				var nogozone = document.createElement('div');
				nogozone.id = "no-go";
				if(settings.debug){
					nogozone.classList.add('debug');
				}
				for(var n=0;n<sceneList[sceneID].bgMap.nogo.length;n++){
					var newZone = document.createElement('div');
					newZone.classList.add('no-go-zone');
					newZone.id = "no-go-zone-"+n;
					newZone.style.top = sceneList[sceneID].bgMap.nogo[n].top+'px';
					newZone.style.left = sceneList[sceneID].bgMap.nogo[n].left+'px';
					newZone.style.height = sceneList[sceneID].bgMap.nogo[n].height+'px';
					newZone.style.width = sceneList[sceneID].bgMap.nogo[n].width+'px';
					nogozone.appendChild(newZone);
				}
				game.world.appendChild(nogozone);
			}
			if(sceneList[sceneID].bgMap.doors){
				var doorLayer = document.createElement('div');
				doorLayer.id = 'doors';
				if(settings.debug){
					doorLayer.classList.add('debug');
				}
				for(var d=0;d<sceneList[sceneID].bgMap.doors.length;d++){
					var newDoor = document.createElement('div');
					newDoor.classList.add('door');
					newDoor.dataset.doorstatus = sceneList[sceneID].bgMap.doors[d].doorstatus;
					if(sceneList[sceneID].bgMap.doors[d].keyneeded !== false){
						newDoor.dataset.key = sceneList[sceneID].bgMap.doors[d].keyneeded;
					}
					if(sceneList[sceneID].bgMap.doors[d].gotoscene){
						newDoor.dataset.gotoscene = sceneList[sceneID].bgMap.doors[d].gotoscene;
						if(sceneList[sceneID].bgMap.doors[d].returnstartpoint){
							newDoor.dataset.returnstartpoint = sceneList[sceneID].bgMap.doors[d].returnstartpoint;
						}
					}
					if(sceneList[sceneID].bgMap.doors[d].gotopoint){
						newDoor.dataset.gotopoint = sceneList[sceneID].bgMap.doors[d].gotopoint;
					}
					newDoor.id = "door-"+d;
					newDoor.style.top = sceneList[sceneID].bgMap.doors[d].loc.top+'px';
					newDoor.style.left = sceneList[sceneID].bgMap.doors[d].loc.left+'px';
					newDoor.style.height = sceneList[sceneID].bgMap.doors[d].loc.height+'px';
					newDoor.style.width = sceneList[sceneID].bgMap.doors[d].loc.width+'px';
					doorLayer.appendChild(newDoor);
				}
				game.world.appendChild(doorLayer);
			}
			if(sceneList[sceneID].bgMap.items){
				var itemLayer = document.createElement('div');
				itemLayer.id = 'items';
				if(settings.debug){
					itemLayer.classList.add('debug');
				}
				for(var i=0;i<sceneList[sceneID].bgMap.items.length;i++){
					var item = document.createElement('div');
						item.id = "item-"+i;
						item.classList.add('item-sprite');
						item.dataset.item = sceneList[sceneID].bgMap.items[i].item;
						item.dataset.itemkey = i;
						item.style.top = sceneList[sceneID].bgMap.items[i].location[0]+'px';
						item.style.left = sceneList[sceneID].bgMap.items[i].location[1]+'px';
					var itemimage  = document.createElement('img');
						itemimage.src = sceneList[sceneID].bgMap.items[i].sprite;
					item.appendChild(itemimage);
					itemLayer.appendChild(item);
				}
				game.world.appendChild(itemLayer);
			}
		}
		if(sceneList[sceneID].jsRun){
			sceneList[sceneID].jsRun();
		}
		if(startPoint){
			scene.moveChars(startPoint)
//			hud.updateHud('location', sceneList[sceneID].label);
		}else if(game.gameBegan){

			console.log('MOVE TO', sceneList[sceneID].playerStartingPoints)

			scene.moveChars(sceneList[sceneID].playerStartingPoints);
//			hud.updateHud('location', sceneList[sceneID].label);
		}
		
	},
	moveChars(data){
		for(var p=0;p<players.length;p++){
			if(players[p].isActive){
				players[p].sprite.style.top = data[p][0]+'px';
				players[p].sprite.style.left = data[p][1]+'px';
			}
		}
	}
}
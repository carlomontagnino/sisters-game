const game = {
	stage : document.getElementById('stage'),
	world : document.getElementById('world'),
	hud : document.getElementById('hud'),
	sprites : document.getElementById('sprites'),
	currentScene : false,
	inDialog : false,
	inMenu : true,
	isPaused : false,
	gameBegan : false,
	step : 1,
	collisionDivisor : 4,
}
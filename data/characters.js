const sisters = [
	{
		name : "Sofia",
		description : "Sofia enjoys puzzles and is an excellent problem solver.<br>She is smart and kind.",
		color: "green",
		abilities : [
			{
				name : 'Heal',
				effect : 'increaseHP',
				amount : 25,
				target : 'sister',
				cost : 10
			},
			{
				name : 'Solve',
				effect : 'solvePuzzle',
				cost : 30
			}
		],
		currentHP : 100,
		selected : false,
		spriteURI : {
			base : '/assets/sprites/Sofia-Front.gif',
			walkingdown : '/assets/sprites/Sofia-Front-Walk.gif',
			walkingup : '/assets/sprites/Sofia-Back-Walk.gif',
			walking : '/assets/sprites/Sofia-Side-Walk.gif'
		}
	},
	{
		name : "Romy",
		description : "Romy enjoys athletics and is a natural leader.<br>She is strong and brave.",
		color: "blue",
		abilities : [
			{
				name : 'Break',
				effect : 'causeDamage',
				amount : 25,
				target : 'enemy',
				cost : 10
			},
			{
				name : 'Rally',
				effect : 'costFreeAbility',
				target : 'sister',
				cost : 30
			}
		],
		currentHP : 100,
		selected : false,
		spriteURI : {
			base : '/assets/sprites/Romy-Front.gif',
			walkingdown : '/assets/sprites/Romy-Front-Walk.gif',
			walkingup : '/assets/sprites/Romy-Back-Walk.gif',
			walking : '/assets/sprites/Romy-Side-Walk.gif'
		}
	},
	{
		name : "Hannah",
		description : "Hannah enjoys playing and is always happy.<br>She is resilient and funny.",
		color: "pink",
		abilities : [
			{
				name : 'Absorb',
				effect : 'limitDamage',
				amount : 25,
				target : 'self',
				cost : 10
			},
			{
				name : 'Distract',
				effect : 'removeAbility',
				target : 'enemy',
				cost : 30
			}
		],
		currentHP : 100,
		selected : false,
		spriteURI : {
			base : '/assets/sprites/Hannah-Front.gif',
			walkingdown : '/assets/sprites/Hannah-Front-Walk.gif',
			walkingup : '/assets/sprites/Hannah-Back-Walk.gif',
			walking : '/assets/sprites/Hannah-Side-Walk.gif'
		}
	}
];

const baseAbilities = [
	{
		name : 'Attack',
		effect : 'causeDamage',
		amount : 5,
		target : 'enemy',
		cost : 0
	},
	{
		name : 'Use Item',
		effect : 'useItem',
		cost : 0
	}
];
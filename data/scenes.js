const sceneList = {
	intro : {
		html : '<h1>Sisters!</h1><h2>Press Start to begin.</h2>',
	},
	characterSelect : {
		html : '<h3>Select a Character</h3><div id="characterSelectList"></div><h5>Press A to choose your character, then Start to begin game.</h5>',
		jsRun : function(){
			game.inMenu = true;
			var parentWrap = document.getElementById('characterSelectList');
			for(var s=0;s<sisters.length;s++){
				var container = document.createElement('div');
					container.classList.add('character');
					container.dataset.character = sisters[s].name;
					container.dataset.player = false;
			//		for(var ss=0;ss<sisters.length;ss++){
			//			container.dataset['player'+(ss+1)] = false;
			//		}
				var inner = document.createElement('div');
					inner.classList.add('character-inner');
				var charImg = document.createElement('img');
					charImg.dataset.bordercolor = sisters[s].color;
					charImg.src = sisters[s].spriteURI.base;
				var charName = document.createElement('h4');
					charName.innerText = sisters[s].name;
				var charDescription = document.createElement('p');
					charDescription.class = "description";
					charDescription.innerHTML = sisters[s].description;
				inner.appendChild(charImg);
				inner.appendChild(charName);
				container.appendChild(inner);
				container.appendChild(charDescription);
				parentWrap.appendChild(container);
			}
			for(var p=0;p<players.length;p++){
				players[p].cycleSisterSelect(false);
			}
		}
	},
	outside : {
		label : "Outside, House",
		bgMap : {
			src : "/assets/backgrounds/house/outside.gif",
			width:"800px",
			mapArea : [0,800,800,0],
			doors : [
				{
					loc : {
						top: 139,
						left: 123,
						height: 45,
						width: 31
					},
					doorstatus : 'closed',
					keyneeded: false,
					gotoscene : false,
					gotopoint : [
						[444,266],
						[444,384],
						[444,502]
					]
				},
				{
					loc : {
						top: 139,
						left: 215,
						height: 45,
						width: 31
					},
					doorstatus : 'open',
					keyneeded: false,
					gotoscene : 'room1',
					gotopoint : false
				},
				{
					loc : {
						top: 139,
						left: 338,
						height: 45,
						width: 31
					},
					doorstatus : 'closed',
					keyneeded: false,
					gotoscene : false,
					gotopoint : false
				},
				{
					loc : {
						top: 139,
						left: 461,
						height: 45,
						width: 31
					},
					doorstatus : 'open',
					keyneeded: false,
					gotoscene : 'room3',
					gotopoint : false
				},
				{
					loc : {
						top: 262,
						left: 615,
						height: 45,
						width: 31
					},
					doorstatus : 'open',
					keyneeded: false,
					gotoscene : 'room4',
					gotopoint : false
				},
				{
					loc : {
						top: 262,
						left: 707,
						height: 45,
						width: 31
					},
					doorstatus : 'open',
					keyneeded: false,
					gotoscene : 'room5',
					gotopoint : false
				},
				{
					loc : {
						top: 293,
						left: 123,
						height: 45,
						width: 31
					},
					doorstatus : 'closed',
					keyneeded: false,
					gotoscene : false,
					gotopoint : false
				},
				{
					loc : {
						top: 293,
						left: 215,
						height: 45,
						width: 31
					},
					doorstatus : 'closed',
					keyneeded: false,
					gotoscene : false,
					gotopoint : false
				},
				{
					loc : {
						top: 293,
						left: 462,
						height: 45,
						width: 31
					},
					doorstatus : 'open',
					keyneeded: false,
					gotoscene : 'room8',
					gotopoint : false
				},
				{
					loc : {
						top: 416,
						left: 615,
						height: 45,
						width: 31
					},
					doorstatus : 'closed',
					keyneeded: false,
					gotoscene : false,
					gotopoint : false
				},
				{
					loc : {
						top: 416,
						left: 708,
						height: 45,
						width: 31
					},
					doorstatus : 'closed',
					keyneeded: false,
					gotoscene : false,
					gotopoint : false
				},
				{
					loc : {
						top: 554,
						left: 369,
						height: 45,
						width: 31
					},
					doorstatus : 'closed',
					keyneeded: false,
					gotoscene : 'city',
					gotopoint : false
				}
			],
			nogo : [
				{
					top: 0,
					left: 0,
					height: 184,
					width: 768
				},
				{
					top: 184,
					left: 0,
					height: 155,
					width: 100
				},
				{
					top: 184,
					left: 277,
					height: 32,
					width: 61
				},
				{
					top: 184,
					left: 585,
					height: 123,
					width: 183
				},
				{
					top: 253,
					left: 100,
					height: 86,
					width: 238
				},
				{
					top: 246,
					left: 331,
					height: 7,
					width: 7
				},
				{
					top: 277,
					left: 338,
					height: 92,
					width: 8
				},
				{
					top: 339,
					left: 277,
					height: 30,
					width: 61
				},
				{
					top: 336,
					left: 400,
					height: 33,
					width: 61
				},
				{
					top: 277,
					left: 392,
					height: 92,
					width: 8
				},
				{
					top: 246,
					left: 400,
					height: 7,
					width: 7
				},
				{
					top: 252,
					left: 400,
					height: 84,
					width: 131
				},
				{
					top: 336,
					left: 496,
					height: 63,
					width: 35
				},
				{
					top: 375,
					left: 523,
					height: 86,
					width: 245
				},
				{
					top: 307,
					left: 762,
					height: 68,
					width: 6
				},
				{
					top: 339,
					left: 0,
					height: 261,
					width: 62
				},
				{
					top: 434,
					left: 62,
					height: 166,
					width: 305
				},
				{
					top: 434,
					left: 402,
					height: 166,
					width: 29
				},
				{
					top: 528,
					left: 493,
					height: 166,
					width: 30
				},
				{
					top: 467,
					left: 431,
					height: 166,
					width: 31
				},
				{
					top: 528,
					left: 462,
					height: 56,
					width: 31
				},
				{
					top: 559,
					left: 523,
					height: 56,
					width: 30
				},
				{
					top: 528,
					left: 553,
					height: 56,
					width: 31
				},
				{
					top: 559,
					left: 584,
					height: 56,
					width: 30
				},
				{
					top: 528,
					left: 615,
					height: 56,
					width: 31
				},
				{
					top: 498,
					left: 645,
					height: 56,
					width: 31
				},
				{
					top: 498,
					left: 738,
					height: 56,
					width: 31
				},
				{
					top: 474,
					left: 769,
					height: 56,
					width: 31
				},
				{
					top: 528,
					left: 677,
					height: 56,
					width: 61
				},
				{
					top: 406,
					left: 308,
					height: 56,
					width: 31
				},
				{
					top: 406,
					left: 246,
					height: 56,
					width: 31
				},
				{
					top: 406,
					left: 185,
					height: 56,
					width: 31
				},
				{
					top: 406,
					left: 93,
					height: 56,
					width: 31
				},
				{
					top: 373,
					left: 62,
					height: 56,
					width: 31
				}
			],
			items : [
				{
					item : 'apple',
					sprite : '/assets/sprites/item-apple.gif',
					label : 'apple',
					context : 'item',
					message : "You've found an apple.",
					msgOptions : [
						{
							label: 'Keep',
							value: 'keep'	
						},
						{
							label: 'Ignore',
							value : 'ignore'
						},
						{
							label: 'Use',
							value : 'use'
						}
					],
					itemUse : {
						use : 'increaseHP',
						amount : '20'
					},
					location: [345,63]
				}
			],
		},
		playerStartingPoints : [
			[354,142],
			[462,478],
			[188,506]
		]
	},
	room1: {
		label : "Room 1, House",
		bgMap : {
			src : "/assets/backgrounds/house/room1.png",
			width:"523px",
			mapArea : [0,523,598,0],
			nogo : [
				{
					top: 0,
					left: 0,
					height: 600,
					width: 37
				},
				{
					top: 0,
					left: 37,
					height: 74,
					width: 449
				},
				{
					top: 20,
					left: 261,
					height: 279,
					width: 37
				},
				{
					top: 0,
					left: 486,
					height: 600,
					width: 37
				},
				{
					top: 336,
					left: 37,
					height: 74,
					width: 187
				},
				{
					top: 336,
					left: 262,
					height: 74,
					width: 224
				},
				{
					top: 410,
					left: 150,
					height: 75,
					width: 36
				},
				{
					top: 523,
					left: 37,
					height: 77,
					width: 187
				},
				{
					top: 523,
					left: 262,
					height: 77,
					width: 224
				},
				{
					top: 74,
					left: 224,
					height: 55,
					width: 37
				},
				{
					top: 151,
					left: 224,
					height: 55,
					width: 37
				},
				{
					top: 150,
					left: 112,
					height: 74,
					width: 74
				},
				{
					top: 74,
					left: 298,
					height: 19,
					width: 39
				},
				{
					top: 74,
					left: 413,
					height: 18,
					width: 73
				},
				{
					top: 92,
					left: 448,
					height: 39,
					width: 38
				},
				{
					top:229,
					left: 414,
					height: 70,
					width: 72
				},
				{
					top:410,
					left: 337,
					height: 19,
					width: 149
				},
				{
					top:410,
					left: 77,
					height: 19,
					width: 33
				},
				{
					top:410,
					left: 114,
					height: 9,
					width: 33
				}
			],
			doors : [
				{
					loc : {
						top: 554,
						left: 225,
						height: 52,
						width: 36
					},
					doorstatus : 'open',
					keyneeded: false,
					gotoscene : 'outside',
					returnstartpoint : [[149,214],[149,214],[149,214]]
				},
			],
			items : []
		},
		playerStartingPoints : [
			[510,226],
			[510,226],
			[510,226]
		]
	},
	room3: {
		label : "Room 3, House",
		bgMap : {
			src : "/assets/backgrounds/house/room3.png",
			width:"523px",
			mapArea : [0,523,598,0],
			nogo : [
				{
					top: 0,
					left: 0,
					height: 600,
					width: 37
				},
				{
					top: 0,
					left: 37,
					height: 74,
					width: 449
				},
				{
					top: 20,
					left: 261,
					height: 279,
					width: 37
				},
				{
					top: 0,
					left: 486,
					height: 600,
					width: 37
				},
				{
					top: 336,
					left: 37,
					height: 74,
					width: 187
				},
				{
					top: 336,
					left: 262,
					height: 74,
					width: 224
				},
				{
					top: 410,
					left: 150,
					height: 75,
					width: 36
				},
				{
					top: 523,
					left: 37,
					height: 77,
					width: 187
				},
				{
					top: 523,
					left: 262,
					height: 77,
					width: 224
				},
				{
					top: 74,
					left: 224,
					height: 17,
					width: 37
				},
				{
					top: 150,
					left: 112,
					height: 74,
					width: 74
				},
				{
					top: 74,
					left: 298,
					height: 19,
					width: 39
				},
				{
					top: 74,
					left: 413,
					height: 18,
					width: 73
				},
				{
					top: 92,
					left: 448,
					height: 39,
					width: 38
				},
				{
					top:410,
					left: 337,
					height: 19,
					width: 149
				},
				{
					top:410,
					left: 77,
					height: 19,
					width: 33
				},
				{
					top:410,
					left: 114,
					height: 9,
					width: 33
				}
			],
			doors : [
				{
					loc : {
						top: 554,
						left: 225,
						height: 52,
						width: 36
					},
					doorstatus : 'open',
					keyneeded: false,
					gotoscene : 'outside',
					returnstartpoint : [[149,460],[149,460],[149,460]]
				},
			],
			items : []
		},
		playerStartingPoints : [
			[510,226],
			[510,226],
			[510,226]
		]
	},
	room4: {
		label : "Room 4, House",
		bgMap : {
			src : "/assets/backgrounds/house/room4.png",
			width:"523px",
			mapArea : [0,523,598,0],
			nogo : [
				{
					top: 0,
					left: 0,
					height: 600,
					width: 37
				},
				{
					top: 0,
					left: 37,
					height: 74,
					width: 449
				},
				{
					top: 20,
					left: 261,
					height: 279,
					width: 37
				},
				{
					top: 0,
					left: 486,
					height: 600,
					width: 37
				},
				{
					top: 336,
					left: 37,
					height: 74,
					width: 187
				},
				{
					top: 336,
					left: 262,
					height: 74,
					width: 224
				},
				{
					top: 410,
					left: 150,
					height: 75,
					width: 36
				},
				{
					top: 523,
					left: 37,
					height: 77,
					width: 187
				},
				{
					top: 523,
					left: 262,
					height: 77,
					width: 224
				},
				{
					top: 150,
					left: 112,
					height: 74,
					width: 74
				},
				{
					top: 74,
					left: 298,
					height: 19,
					width: 39
				},
				{
					top: 74,
					left: 37,
					height: 19,
					width: 35
				},
				{
					top: 74,
					left: 448,
					height: 58,
					width: 38
				},
				{
					top:149,
					left: 336,
					height: 75,
					width: 75
				},
				{
					top:410,
					left: 337,
					height: 19,
					width: 149
				},
				{
					top:410,
					left: 77,
					height: 19,
					width: 33
				},
				{
					top:410,
					left: 114,
					height: 9,
					width: 33
				}
			],
			doors : [
				{
					loc : {
						top: 554,
						left: 225,
						height: 52,
						width: 36
					},
					doorstatus : 'open',
					keyneeded: false,
					gotoscene : 'outside',
					returnstartpoint : [[276,614],[276,614],[276,614]]
				},
			],
			items : []
		},
		playerStartingPoints : [
			[510,226],
			[510,226],
			[510,226]
		]
	},
	room5: {
		label : "Room 5, House",
		bgMap : {
			src : "/assets/backgrounds/house/room5.png",
			width:"523px",
			mapArea : [0,523,598,0],
			nogo : [
				{
					top: 0,
					left: 0,
					height: 600,
					width: 37
				},
				{
					top: 0,
					left: 37,
					height: 74,
					width: 449
				},
				{
					top: 20,
					left: 261,
					height: 279,
					width: 37
				},
				{
					top: 0,
					left: 486,
					height: 600,
					width: 37
				},
				{
					top: 336,
					left: 37,
					height: 74,
					width: 187
				},
				{
					top: 336,
					left: 262,
					height: 74,
					width: 224
				},
				{
					top: 410,
					left: 150,
					height: 75,
					width: 36
				},
				{
					top: 523,
					left: 37,
					height: 77,
					width: 187
				},
				{
					top: 523,
					left: 262,
					height: 77,
					width: 224
				},
				{
					top: 74,
					left: 224,
					height: 17,
					width: 37
				},
				{
					top: 150,
					left: 112,
					height: 74,
					width: 74
				},
				{
					top: 74,
					left: 298,
					height: 19,
					width: 39
				},
				{
					top: 74,
					left: 413,
					height: 18,
					width: 73
				},
				{
					top: 92,
					left: 448,
					height: 39,
					width: 38
				},
				{
					top:410,
					left: 337,
					height: 19,
					width: 149
				},
				{
					top:410,
					left: 77,
					height: 19,
					width: 33
				},
				{
					top:410,
					left: 114,
					height: 9,
					width: 33
				}
			],
			doors : [
				{
					loc : {
						top: 554,
						left: 225,
						height: 52,
						width: 36
					},
					doorstatus : 'open',
					keyneeded: false,
					gotoscene : 'outside',
					returnstartpoint : [[276,706],[276,706],[276,706]]
				}
			],
			items : []
		},
		playerStartingPoints : [
			[510,226],
			[510,226],
			[510,226]
		]
	},
	room8 : {
		label : "Room 8, House",
		bgMap : {
			src : "/assets/backgrounds/house/room8.png",
			width:"523px",
			mapArea : [0,523,598,0],
			nogo : [
				{
					top: 0,
					left: 0,
					height: 600,
					width: 37
				},
				{
					top: 0,
					left: 37,
					height: 74,
					width: 449
				},
				{
					top: 20,
					left: 261,
					height: 279,
					width: 37
				},
				{
					top: 0,
					left: 486,
					height: 600,
					width: 37
				},
				{
					top: 336,
					left: 37,
					height: 74,
					width: 187
				},
				{
					top: 336,
					left: 262,
					height: 74,
					width: 224
				},
				{
					top: 410,
					left: 150,
					height: 75,
					width: 36
				},
				{
					top: 523,
					left: 37,
					height: 77,
					width: 187
				},
				{
					top: 523,
					left: 262,
					height: 77,
					width: 224
				},
				{
					top: 150,
					left: 112,
					height: 74,
					width: 74
				},
				{
					top: 74,
					left: 298,
					height: 19,
					width: 39
				},
				{
					top: 74,
					left: 413,
					height: 18,
					width: 73
				},
				{
					top: 92,
					left: 448,
					height: 39,
					width: 38
				},
				{
					top:453,
					left: 413,
					height: 70,
					width: 73
				},
				{
					top:410,
					left: 302,
					height: 19,
					width: 184
				},
				{
					top:410,
					left: 77,
					height: 19,
					width: 33
				},
				{
					top:410,
					left: 114,
					height: 9,
					width: 33
				}
			],
			doors : [
				{
					loc : {
						top: 554,
						left: 225,
						height: 52,
						width: 36
					},
					doorstatus : 'open',
					keyneeded: false,
					gotoscene : 'outside',
					returnstartpoint : [[305,461],[305,461],[305,461]]
				},
			],
			items : []
		},
		playerStartingPoints : [
			[510,226],
			[510,226],
			[510,226]
		]
	}
}
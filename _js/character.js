const character = {
	getNextAvailableSister(){
		for(var s=0;s<sisters.length;s++){
			if(!sisters[s].selected){
				return sisters[s];
			}
		}
	},
	cycleAvailableSisters(player){
		for(var s=0;s<sisters.length;s++){
			if(sisters[s].name == player.name){
				console.log(sisters[s].name, player.name);
				if(sisters[s+1]){
					player.name = sisters[s+1].name;
					player.color = sisters[s+1].color;
					player.ability = sisters[s+1].ability;
					player.sprite.dataset.character = sisters[s+1].name;
				}else{
					player.name = sisters[0].name;
					player.color = sisters[0].color;
					player.ability = sisters[0].ability;
					player.sprite.dataset.character = sisters[0].name;
				}
			}
		}
	}
}
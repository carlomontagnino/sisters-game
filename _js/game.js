const game = {
	stage : document.getElementById('stage'),
	world : document.getElementById('world'),
	hud : document.getElementById('hud'),
	spritePlane : document.getElementById('sprites'),
	currentScene : 'intro',
	inDialog : false,
	isPaused : false,
	players : [],
	createPlayer(index, gamepad){
		//console.log('createPlayer', gamepad);
		if(!game.determinePlayer((index))){
			var playerObj = {
				isActive : true,
				id : "player_"+(index+1),
				controls : gamepad,
				items : [],
				indoorway : false,
				name : sisters[0].name,
				color : sisters[0].color,
				ability : sisters[0].ability
			}
			var spriteRaw = document.createElement('div');
			spriteRaw.id = playerObj.id;
			spriteRaw.classList.add('player');
			spriteRaw.dataset.indoorway = false;
			spriteRaw.dataset.playernumber = index;
			spriteRaw.dataset.character = playerObj.name;
			game.spritePlane.appendChild(spriteRaw);
			playerObj.sprite = document.getElementById(playerObj.id);
			players.push(playerObj);
			game.moveChars(sceneList[game.currentScene].playerStartingPoints);
		}
	},
	determinePlayer(index){
		if(game.players[index]){
			return game.players[index];
		}
		console.warn('There is no Player number '+ (index))
		return false;
	},
	playerHasItem(player, itemName){
		for(var i=0;i<player.items.length;i++){
			if(player.items[i].name == itemName){
				return true;
			}
		}
	},
	buttonMap : {0:"B",1:"A",8:"SELECT",9:"START",12:"UP",13:"DOWN",14:"LEFT",15:"RIGHT"},
	doGameAction(info){
		var player = game.players[info.gamepad.index];

		if(player){
			var sprite = player.sprite;
			var buttons = [];
			for(var b=0;b<info.gamepad.buttons.length;b++){
				if(info.gamepad.buttons[b].pressed){
					buttons.push(buttonMap[b]);
				}
			}
			if(buttons.indexOf("B") != -1){ 
				if(currentScene != 'intro'){
					if(inDialog){
						// check for options, if there are options, this will confirm it. otherwise:
						closeDialogBox();
					}
				}
			}
			if(buttons.indexOf("A") != -1){ 
			//	console.log('PLAYER', player)
			//	console.log('SPRITE', sprite)
				if(player.indoorway){
				//	console.log('DOOR', player.indoorway)
					checkDoor(player.indoorway, sprite);
				}
				if(currentScene == 'intro'){
				//	console.log('GET CURRENT PLAYER')

				}
			}
			if(buttons.indexOf("UP") != -1){ 
				if(
					(currentScene != 'intro') &&
					!inDialog
				){
					walk('UP', sprite);
				}
			}
			if(buttons.indexOf("DOWN") != -1){
				if(
					(currentScene != 'intro') &&
					!inDialog
				){
					walk('DOWN', sprite);
				}
			}
			if(buttons.indexOf("LEFT") != -1){ 
				if(
					(currentScene != 'intro') &&
					!inDialog
				){
					walk('LEFT', sprite);
				}
			}
			if(buttons.indexOf("RIGHT") != -1){ 
				if(
					(currentScene != 'intro') &&
					!inDialog
				){
					walk('RIGHT', sprite);
				}
			}
			if(
				(buttons.indexOf("UP") == -1) &&
				(buttons.indexOf("DOWN") == -1) &&
				(buttons.indexOf("LEFT") == -1) &&
				(buttons.indexOf("RIGHT") == -1)
			){ 
				walk('STOP', sprite);
			}
			if(buttons.indexOf("SELECT") != -1){ 
				if(
					(currentScene != 'intro') &&
					!inDialog
				){
					//cycleAvailableSisters(player);
				}
			}
			if(buttons.indexOf("START") != -1){ 
				if(currentScene == 'intro'){
					game.runScene('outside');
				}else if(
					(currentScene != 'intro') &&
					!inDialog
				){

				}
			}
			if(
				(buttons.indexOf("SELECT") != -1) &&
				(buttons.indexOf("START") != -1)
			){ 
				location.reload();
			}
		}
	},
	runScene(scene, startPoint){
		console.log('runScene: ',scene,sceneList[scene]);
		world.innerHTML = '';
		game.stage.dataset.scene = scene;
		if(sceneList[scene].html){
			game.world.innerHTML = sceneList[scene].html;
		}
		if(sceneList[scene].bgMap){
			bgImg = document.createElement('img');
			bgImg.src = sceneList[scene].bgMap.src;
			bgImg.classList.add('bgMap');
			bgImg.id = scene;
			bgImg.style.width = sceneList[scene].bgMap.width;
			game.world.appendChild(bgImg);
			if(sceneList[scene].bgMap.nogo){
				var nogozone = document.createElement('div');
				nogozone.id = "no-go";
				for(var n=0;n<sceneList[scene].bgMap.nogo.length;n++){
					var newZone = document.createElement('div');
					newZone.classList.add('no-go-zone');
					newZone.id = "no-go-zone-"+n;
					newZone.style.top = sceneList[scene].bgMap.nogo[n].top+'px';
					newZone.style.left = sceneList[scene].bgMap.nogo[n].left+'px';
					newZone.style.height = sceneList[scene].bgMap.nogo[n].height+'px';
					newZone.style.width = sceneList[scene].bgMap.nogo[n].width+'px';
					nogozone.appendChild(newZone);
				}
				game.world.appendChild(nogozone);
			}
			if(sceneList[scene].bgMap.doors){
				var doorLayer = document.createElement('div');
				doorLayer.id = 'doors';
				for(var d=0;d<sceneList[scene].bgMap.doors.length;d++){
					var newDoor = document.createElement('div');
					newDoor.classList.add('door');
					newDoor.dataset.doorstatus = sceneList[scene].bgMap.doors[d].doorstatus;
					if(sceneList[scene].bgMap.doors[d].keyneeded !== false){
						newDoor.dataset.key = sceneList[scene].bgMap.doors[d].keyneeded;
					}
					if(sceneList[scene].bgMap.doors[d].gotoscene){
						newDoor.dataset.gotoscene = sceneList[scene].bgMap.doors[d].gotoscene;
						if(sceneList[scene].bgMap.doors[d].returnstartpoint){
							newDoor.dataset.returnstartpoint = sceneList[scene].bgMap.doors[d].returnstartpoint;
						}
					}
					if(sceneList[scene].bgMap.doors[d].gotopoint){
						newDoor.dataset.gotopoint = sceneList[scene].bgMap.doors[d].gotopoint;
					}
					newDoor.id = "door-"+d;
					newDoor.style.top = sceneList[scene].bgMap.doors[d].loc.top+'px';
					newDoor.style.left = sceneList[scene].bgMap.doors[d].loc.left+'px';
					newDoor.style.height = sceneList[scene].bgMap.doors[d].loc.height+'px';
					newDoor.style.width = sceneList[scene].bgMap.doors[d].loc.width+'px';
					doorLayer.appendChild(newDoor);
				}
				game.world.appendChild(doorLayer);
			}
		}
		game.currentScene = scene;
		if(startPoint){
			game.moveChars(startPoint)
		}else{
			game.moveChars(sceneList[scene].playerStartingPoints);
		}
		game.clearCharStageStatus();
	},
	clearCharStageStatus(){
		for(var p=0;p<players.length;p++){
			if(players[p].sprite){
				players[p].indoorway = false;
				players[p].sprite.dataset.indoorway = false;
			}
		}
	},
	moveChars(data){
	//	console.log('moveChars', data);
		for(var p=0;p<players.length;p++){
			if(players[p].isActive){
				players[p].sprite.style.top = data[p][0]+'px';
				players[p].sprite.style.left = data[p][1]+'px';
			}
		}
	}
}
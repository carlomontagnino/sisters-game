document.addEventListener("readystatechange", (event) => {
	if (event.target.readyState === "interactive") {
		
	} else if (event.target.readyState === "complete") {
		ready();
	}
});

// document.addEventListener("DOMContentLoaded", ready);



function ready() {
	if(!settings.debug){
		document.getElementById('game_data').remove();
	}
	window.addEventListener("gamepadconnected", gamepadAPI.handleConnect);
	window.addEventListener("gamepaddisconnected", gamepadAPI.handleDisconnect);
	requestAnimationFrame(gamepadAPI.process);
	scene.loadScene('intro');
}
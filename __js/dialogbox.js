const dialogBox = {
	hasOptions : false,
	closeDialogBox(cb){
		document.getElementById('dialogbox').remove();
		game.inDialog = false;
		dialogBox.hasOptions = false;
		if(cb){
			cb();
		}
	},
	displayDialogBox(message, options){
		var existingDialog = document.getElementById('dialogbox');
		if(!existingDialog){
			var dialogBoxContainer = document.createElement('div');
			dialogBoxContainer.id = 'dialogbox';
			var dialogBoxInner = document.createElement('div');
			dialogBoxInner.id = 'dialogboxinner';
			var dialogBoxMessage = document.createElement('div');
			dialogBoxMessage.id = 'dialogboxmessage';
			dialogBoxMessage.innerText = message;
			dialogBoxInner.appendChild(dialogBoxMessage);
			if(options){
				var dialogBoxOptions = document.createElement('div');
				dialogBoxOptions.id = 'dialogboxoptions';
				for(var o=0;o<options.length;o++){
					var newOption = document.createElement('span');
					newOption.class = "dialogoption";
					newOption.dataset.optionvalue = options[o].value;
					newOption.innerText = options[o].label;
					dialogBoxOptions.appendChild(newOption);
				}
				dialogBoxInner.appendChild(dialogBoxOptions);
				dialogBox.hasOptions = true;
			}else{
				dialogBox.hasOptions = false;
			}
			dialogBoxContainer.appendChild(dialogBoxInner);
			game.hud.appendChild(dialogBoxContainer);
			game.inDialog = true;
			if(dialogBox.hasOptions){
				dialogBox.cycleDialogOptions();
			}
		}
	},
	cycleDialogOptions(dir){
		var options = document.getElementById('dialogboxoptions').childNodes;
		var selectedOption = false;
		var newKey = 0;
		for(var o=0;o<options.length;o++){
			if(options[o].classList.contains('hovered')){
				selectedOption = options[o];
				newKey = o;
			}
		}
		if(!selectedOption){
			options[0].classList.add('hovered');
		}else{
			options[newKey].classList.remove('hovered');
			if(dir == 'prev'){
				if(options[newKey-1]){
					options[newKey-1].classList.add('hovered');
				}else{
					options[options.length-1].classList.add('hovered');
				}
			}
			if(dir == 'next'){
				if(options[newKey+1]){
					options[newKey+1].classList.add('hovered');
				}else{
					options[0].classList.add('hovered');
				}
			}
		}
	},
	selectOption(options){
		var options = document.getElementById('dialogboxoptions').childNodes;
		var selectedOption = false;
		for(var o=0;o<options.length;o++){
			if(options[o].classList.contains('hovered')){
				selectedOption = options[o];
			}
		}
		if(selectedOption){
			console.log(selectedOption.dataset.optionvalue)
			var fn = window[selectedOption.dataset.optionvalue];
			fn();
			
		}
	}
}
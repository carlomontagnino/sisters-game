const move = {
	step : 1,
	collisionDivisor : 4,
	spriteBuffer(boundary, dir){
		switch(dir){
			case 'right':
				return boundary-32;
				break;
			case 'bottom':
				return boundary-48;
				break;
			default :
				return boundary;
		}
	},
	walk(playerIndex, dir){
		var y = players[playerIndex].sprite.offsetTop; 
		var x = players[playerIndex].sprite.offsetLeft; 
		var thisPlayer = player.determinePlayer(playerIndex);
		var nextStep, newPos;
		move.setWalkingClasses(dir, thisPlayer.sprite);
		thisPlayer.indoorway = move.isPlayerInDoor(thisPlayer.sprite);
		if(thisPlayer.indoorway){
			thisPlayer.sprite.dataset.indoorway = true;
		}else{
			thisPlayer.sprite.dataset.indoorway = false;
		}
		thisPlayer.nearitem = move.isPlayerNearItem(thisPlayer.sprite);
		if(thisPlayer.nearitem){
			thisPlayer.sprite.dataset.nearitem = true;
		}else{
			thisPlayer.sprite.dataset.nearitem = false;
		}
		switch(dir){
			case "UP":
				nextStep = y-move.step;
				if(nextStep <= defaultBounds.top){
					nextStep = defaultBounds.top;
				}
				if(move.willHitNoGoZone(thisPlayer.sprite, dir)){
					nextStep = y;
				}
				thisPlayer.sprite.style.top = nextStep+'px';
				break;
			case "DOWN":
				nextStep = y+move.step;
				if(nextStep >= defaultBounds.bottom){
					nextStep = defaultBounds.bottom;
				}
				if(move.willHitNoGoZone(thisPlayer.sprite, dir)){
					nextStep = y;
				}
				thisPlayer.sprite.style.top = nextStep+'px';
				break;
			case "LEFT":
				nextStep = x-move.step;
				if(nextStep <= defaultBounds.left){
					nextStep = defaultBounds.left;
				}
				if(move.willHitNoGoZone(thisPlayer.sprite, dir)){
					nextStep = x;
				}
				thisPlayer.sprite.style.left = nextStep+'px';
				break;
			case "RIGHT":
				nextStep = x+move.step;
				if(nextStep >= defaultBounds.right){
					nextStep = defaultBounds.right;
				}
				if(move.willHitNoGoZone(thisPlayer.sprite, dir)){
					nextStep = x;
				}
				thisPlayer.sprite.style.left = nextStep+'px';
				break;
			case "STOP" : 

				break;
		}
	},
	checkDoor(door, playerIndex){
		if(
			(door.dataset.doorstatus == 'open') ||
			(door.dataset.key && game.hasItem(players[playerIndex], door.dataset.key))
		){
			if(
				(door.dataset.gotopoint) ||
				(door.dataset.gotoscene)
			){
				move.goThroughDoor(door, players[playerIndex].sprite);
			}else{
				dialogBox.displayDialogBox(players[playerIndex].name+": I'm not sure about this...", false);
			}	
		}else{
			dialogBox.displayDialogBox(players[playerIndex].name+": I don't have the key for this door.", false);
		}
	},
	goThroughDoor(door){
		console.log('goThroughDoor', door);
		if(door.dataset.gotopoint){
			var getPoints = door.dataset.gotopoint.split(',');
			var points = [
				[getPoints[0],getPoints[1]],
				[getPoints[2],getPoints[3]],
				[getPoints[4],getPoints[5]]
			];
			game.moveChars(points);
		}else if(door.dataset.gotoscene){
			startPoint = false;
			if(door.dataset.returnstartpoint){
				startPoint = door.dataset.returnstartpoint.split(',');
				var points = [
					[startPoint[0],startPoint[1]],
					[startPoint[2],startPoint[3]],
					[startPoint[4],startPoint[5]]
				];
			}
			scene.loadScene(door.dataset.gotoscene, points);
		}else{
			dialogBox.displayDialogBox("I shouldn't be here.", false);
		}

	},
	setWalkingClasses(dir, sprite){
		dir = dir.toLowerCase();
		sprite.classList.remove('walking');
		sprite.classList.remove('walking-down');
		sprite.classList.remove('walking-left');
		sprite.classList.remove('walking-right');
		sprite.classList.remove('walking-up');
		if(dir != 'stop'){
			sprite.classList.add('walking');
			sprite.classList.add('walking-'+dir);
		}
	},
	willHitNoGoZone(sprite, dir){
		var footline = {
			top : sprite.offsetTop+46,
			bottom : sprite.offsetTop + sprite.offsetHeight,
			left : sprite.offsetLeft,
			right : sprite.offsetLeft + sprite.offsetWidth
		}
		var noGoZones = world.getElementsByClassName('no-go-zone');
		if(noGoZones){
			for(var z=0;z<noGoZones.length;z++){
				var zoneBox = {
					top : noGoZones[z].offsetTop,
					bottom : noGoZones[z].offsetTop + noGoZones[z].offsetHeight,
					left : noGoZones[z].offsetLeft,
					right : noGoZones[z].offsetLeft + noGoZones[z].offsetWidth
				}
				switch(dir){
					case "UP":
						footline.top = footline.top-(move.step/move.collisionDivisor);
						break;
					case "DOWN":
						footline.bottom = footline.bottom+(move.step/move.collisionDivisor);
						break;
					case "LEFT":
						footline.left = footline.left-(move.step/move.collisionDivisor);
						break;
					case "RIGHT":
						footline.right = footline.right+(move.step/move.collisionDivisor);
						break;
				}
				if(
					(footline.top < zoneBox.bottom) &&
					(footline.bottom > zoneBox.top) &&
					(footline.left < zoneBox.right) &&
					(footline.right > zoneBox.left)
				){
					return true;
				}
			}
		}
		
		return false;
	},
	isPlayerInDoor(sprite){
		var spritePos = {
			top : sprite.offsetTop,
			bottom : sprite.offsetTop + sprite.offsetHeight,
			left : sprite.offsetLeft,
			right : sprite.offsetLeft + sprite.offsetWidth
		}
		var doors = game.world.getElementsByClassName('door');
		for(var d=0;d<doors.length;d++){
			var doorBox = {
				top : doors[d].offsetTop,
				bottom : doors[d].offsetTop + doors[d].offsetHeight,
				left : doors[d].offsetLeft,
				right : doors[d].offsetLeft + doors[d].offsetWidth
			}
			if(
				(spritePos.top <= (doorBox.top+5)) &&
				(spritePos.top >= (doorBox.top-5)) &&
				(spritePos.left >= doorBox.left-5) &&
				(spritePos.right <= doorBox.right+5)
			){
				//console.warn('IN DOOR '+d)
				return doors[d];
			}
		}
		return false;
	},
	isPlayerNearItem(sprite){
		var spritePos = {
			top : sprite.offsetTop,
			bottom : sprite.offsetTop + sprite.offsetHeight,
			left : sprite.offsetLeft,
			right : sprite.offsetLeft + sprite.offsetWidth
		}
		var items = game.world.getElementsByClassName('item-sprite');
		for(var i=0;i<items.length;i++){
			var itemBox = {
				top : items[i].offsetTop,
				bottom : items[i].offsetTop + items[i].offsetHeight,
				left : items[i].offsetLeft,
				right : items[i].offsetLeft + items[i].offsetWidth
			}
			if(
				(spritePos.top-48 <= itemBox.top) &&
				(spritePos.bottom+10 >= itemBox.top) &&
				(spritePos.left-10 <= itemBox.left) &&
				(spritePos.right+10 >= itemBox.right)
			){
				return items[i];
			}
		}
		return false;
	},
	interactWithItem(itemSprite, playerIndex){
		var item = sceneList[game.currentScene].bgMap.items[itemSprite.dataset.itemkey];
		console.log('interactWithItem', item);
		if(item.message){
			dialogBox.displayDialogBox(item.message, item.msgOptions);
		}else{
			// add to inventory
		}



	}
}
defaultBounds = {
	top : move.spriteBuffer(0,'top'),
	right: move.spriteBuffer(800,'right'), 
	bottom: move.spriteBuffer(600,'bottom'), 
	left: move.spriteBuffer(0,'left')
};
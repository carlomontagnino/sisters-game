const menu = {
	cycleCharacterSelect(dir, playerIndex, firstTime){
		console.log('cycleCharacterSelect', dir, playerIndex, firstTime);
		var characters = document.getElementsByClassName('character');
		var newKey,hoveredKey;
		var noHovered = true;
		for(var c=0;c<characters.length;c++){
			if(
				(characters[c].classList.contains('hovered')) &&
				(characters[c].dataset.player == playerIndex+1)
			){
				hoveredKey = c;
				noHovered = false;
				characters[c].classList.remove('hovered');
				characters[c].dataset.player = false;
			}
		}
		if(noHovered){
			newKey = 0;
		}else{
			if(dir == 'next'){
				if(characters[hoveredKey+1]){
					newKey = hoveredKey+1;
				}else{
					newKey = 0;
				}
			}
			if(dir == 'prev'){
				if(characters[hoveredKey-1]){
					newKey = hoveredKey-1;
				}else{
					newKey = characters.length-1
				}
			}
		}
		
		characters[newKey].classList.add('hovered');
		characters[newKey].dataset.player = playerIndex+1;
	},
	selectCharacter(playerIndex){
		var characters = document.getElementsByClassName('character');
		for(var c=0;c<characters.length;c++){
			characters[c].classList.remove('selected');
			if(characters[c].dataset.player == (playerIndex+1)){
				players[playerIndex].name = sisters[c].name;
				players[playerIndex].color = sisters[c].color;
				players[playerIndex].ability = sisters[c].ability;
				players[playerIndex].sisterIndex = c.toString();
				players[playerIndex].images = sisters[c].sprite;
				characters[c].classList.add('selected');
			}
		}
	}
}
var players = [];

const player = {
	createPlayer(index, gamepad){
		if(!player.determinePlayer((index))){
			var playerObj = {
				isActive : true,
				id : "player_"+(index+1),
				controls : gamepad,
				items : [],
				indoorway : false,
				name : false,
				color : false,
				ability : false,
				buttonCache : [],
				buttonStatus : [],
				axesStatus : [],
				images : {}
			}
			players.push(playerObj);
		}
	},
	determinePlayer(index){
		if(players[index]){
			return players[index];
		}
		//console.warn('There is no Player number '+ index)
		return false;
	}
}
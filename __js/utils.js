const utils = {
	areValuesTheSame(a, b) {
		return a.sort().join(' ') === b.sort().join(' ');
	}
}
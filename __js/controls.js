const controls = {

	/*
	While we don't use allButtons, it's there for reference. 
	
	buttonMap is created using a classic NES controller. Perhaps I can later detect the controller and apply a custom buttonMap.
	*/
	allButtons: ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16'],
	buttonMap : {0:"B",1:"A",8:"SELECT",9:"START",12:"UP",13:"DOWN",14:"LEFT",15:"RIGHT"},


	/*
	press() checks if the player exists (it should, except for the initial connection) and then digests the buttons into human readable data.

	it then checks if we are in a menu or dialog screen (to prevent constant firing of intended single d-pad presses). IT also prevents the same issue in all non d-pad buttons.

	finally, we pass the data to doAction() and store the button presses in the player's button cache. 

	*/
	press(info){
		if(players[info.gamepad.index]){
			var buttonsPressed = [];
			if(info.gamepad.buttons){
				for(var b=0;b<info.gamepad.buttons.length;b++){
					if(info.gamepad.buttons[b].pressed){
						buttonsPressed.push(controls.buttonMap[b]);
					}
				}
			}
			if(
				((buttonsPressed.indexOf("UP") == -1) && (buttonsPressed.indexOf("DOWN") == -1) && (buttonsPressed.indexOf("LEFT") == -1) && (buttonsPressed.indexOf("RIGHT") == -1)) ||
				(game.inMenu || game.inDialog || game.isPaused) 
			){ 
				// prevent the polling of button pressed over the duration of a hold (no matter how short)
				if(!utils.areValuesTheSame(players[info.gamepad.index].buttonCache,buttonsPressed)){
					controls.doAction(info.gamepad.index, buttonsPressed);
				}
			}else{
				controls.doAction(info.gamepad.index, buttonsPressed);
			}
			players[info.gamepad.index].buttonCache = buttonsPressed;
		}
	},


	/*
		doAction() takes input from press() and routes it to the correct end result. 
	*/
	doAction(playerIndex, buttonPress){

		if(buttonPress.indexOf("UP") != -1){ 
			if(game.currentScene == 'characterSelect'){
				menu.cycleCharacterSelect('prev', playerIndex);				
			}else if(game.inMenu || game.inDialog || game.isPaused){

			}else if(game.gameBegan){
				move.walk(playerIndex, 'UP');
			}
		}

		if(buttonPress.indexOf("DOWN") != -1){
			if(game.currentScene == 'characterSelect'){
				menu.cycleCharacterSelect('next', playerIndex);				
			}else if(game.inMenu || game.inDialog || game.isPaused){
				
			}else if(game.gameBegan){
				move.walk(playerIndex, 'DOWN');
			}
		}

		if(buttonPress.indexOf("LEFT") != -1){ 
			if(game.inMenu || game.isPaused){
				
			}else if(game.inDialog){
				dialogBox.cycleDialogOptions('prev');
			}else if(game.gameBegan){
				move.walk(playerIndex, 'LEFT');
			}
		}

		if(buttonPress.indexOf("RIGHT") != -1){ 
			if(game.inMenu || game.isPaused){
				
			}else if(game.inDialog){
				dialogBox.cycleDialogOptions('next');
			}else if(game.gameBegan){
				move.walk(playerIndex, 'RIGHT');
			}
		}

		if(
			(buttonPress.indexOf("UP") == -1) &&
			(buttonPress.indexOf("DOWN") == -1) &&
			(buttonPress.indexOf("LEFT") == -1) &&
			(buttonPress.indexOf("RIGHT") == -1)
		){ 
			if(game.gameBegan){
				move.walk(playerIndex, 'STOP');
			}
		}

		if(buttonPress.indexOf("B") != -1){ 
			if(game.inMenu || game.isPaused){
				
			}else if(game.inDialog){
				
			}else if(game.gameBegan){
				
			}
		}

		if(buttonPress.indexOf("A") != -1){ 
			if(game.currentScene == 'characterSelect'){
				menu.selectCharacter(playerIndex);
			}else if(game.inDialog){
				if(dialogBox.hasOptions){
					dialogBox.selectOption();
				}else{
					dialogBox.closeDialogBox();
				}
			}else if(game.inMenu || game.isPaused){

			}else if(game.gameBegan){
				if(players[playerIndex].indoorway){
					move.checkDoor(players[playerIndex].indoorway, playerIndex);
				}
				if(players[playerIndex].nearitem){
					move.interactWithItem(players[playerIndex].nearitem, playerIndex);
				}
			}
		}

		if(buttonPress.indexOf("SELECT") != -1){ 
			// MENU or SETTINGS, CHAR CHANGE?
			if(game.inMenu || game.isPaused){
				
			}else if(game.inDialog){
				
			}else if(game.gameBegan){
				
			}
		}

		if(buttonPress.indexOf("START") != -1){ 
			if(game.currentScene == 'intro'){
				scene.loadScene('characterSelect');
			}
			if(game.currentScene == 'characterSelect'){
				game.gameBegan = true;
				// LOOK AT THIS
				for(var p=0;p<players.length;p++){
					if(!players[p].sisterIndex){
						game.gameBegan = false;
					}
				}
				if(game.gameBegan){
					game.inMenu = false;
					sprites.createPlayerSprite(playerIndex);
					hud.createHud();
					scene.loadScene('outside');
				}
			}else if(game.gameBegan){
				// DO PAUSE HERE
			}
		}

		if(
			(buttonPress.indexOf("SELECT") != -1) &&
			(buttonPress.indexOf("START") != -1)
		){ 
			location.reload();
		}
	}
}
const gamepadAPI = {
	fudgeFactor : 2,
	runningElem :  document.querySelector('#running'),
	gamepadsElem : document.querySelector('#gamepads'),
	gamepadsByIndex : {},
	controllerTemplate : `<div><div class="head"><div class="index"></div><div class="id"></div></div><div class="info"><div class="label">connected:</div><span class="connected"></span></div><div class="info"><div class="label">mapping:</div><span class="mapping"></span></div><div class="inputs"><div class="axes"></div><div class="buttons"></div></div></div>`,
	axisTemplate : `<svg viewBox="-2.2 -2.2 4.4 4.4" width="128" height="128"><circle cx="0" cy="0" r="2" fill="none" stroke="#888" stroke-width="0.04" /><path d="M0,-2L0,2M-2,0L2,0" stroke="#888" stroke-width="0.04" /><circle cx="0" cy="0" r="0.22" fill="red" class="axis" /><text text-anchor="middle" fill="#CCC" x="0" y="2">0</text></svg>`,
	buttonTemplate : `<svg viewBox="-2.2 -2.2 4.4 4.4" width="64" height="64"><circle cx="0" cy="0" r="2" fill="none" stroke="#888" stroke-width="0.1" /><circle cx="0" cy="0" r="0" fill="none" fill="red" class="button" /><text class="value" dominant-baseline="middle" text-anchor="middle" fill="#CCC" x="0" y="0">0.00</text><text class="index" alignment-baseline="hanging" dominant-baseline="hanging" text-anchor="start" fill="#CCC" x="-2" y="-2">0</text></svg>`,
	debugWrap : ``,
	addGamepad(gamepad){
		console.log('add:', gamepad.index);
		if(settings.debug){
			const elem = document.createElement('div');
			elem.innerHTML = gamepadAPI.controllerTemplate;
			const axesElem = elem.querySelector('.axes');
			const buttonsElem = elem.querySelector('.buttons');
		}
		const axes = [];
		for (let ndx = 0; ndx < gamepad.axes.length; ndx += 2) {
			if(settings.debug){
				const div = document.createElement('div');
				div.innerHTML = gamepadAPI.axisTemplate;
				axesElem.appendChild(div);
				axes.push({
					axis: div.querySelector('.axis'),
					value: div.querySelector('text'),
				});
			}
			
		}
		const buttons = [];
		for (let ndx = 0; ndx < gamepad.buttons.length; ++ndx) {
			if(settings.debug){
				const div = document.createElement('div');
				div.innerHTML = gamepadAPI.buttonTemplate;
				buttonsElem.appendChild(div);
				div.querySelector('.index').textContent = ndx;
				buttons.push({
					circle: div.querySelector('.button'),
					value: div.querySelector('.value'),
				});
			}
			
		}

		gamepadAPI.gamepadsByIndex[gamepad.index] = {
			gamepad,
			axes,
			buttons
		};

		if(settings.debug){
			gamepadAPI.gamepadsByIndex[gamepad.index].elem;
			gamepadAPI.gamepadsByIndex[gamepad.index].index = elem.querySelector('.index');
			gamepadAPI.gamepadsByIndex[gamepad.index].id = elem.querySelector('.id');
			gamepadAPI.gamepadsByIndex[gamepad.index].mapping = elem.querySelector('.mapping');
			gamepadAPI.gamepadsByIndex[gamepad.index].connected = elem.querySelector('.connected');
			gamepadAPI.gamepadsElem.appendChild(elem);
		}	
		
		player.createPlayer(gamepad.index, gamepad);
	},
	removeGamepad(gamepad) {
		const info = gamepadsByIndex[gamepad.index];
		if (info) {
			delete gamepadsByIndex[gamepad.index];
			info.elem.parentElement.removeChild(info.elem);
		}
	},
	addGamepadIfNew(gamepad) {
		const info = gamepadAPI.gamepadsByIndex[gamepad.index];
		if (!info) {
			gamepadAPI.addGamepad(gamepad);
		} else {
			// This broke sometime in the past. It used to be
			// the same gamepad object was returned forever.
			// Then Chrome only changed to a new gamepad object
			// is returned every frame.
			info.gamepad = gamepad;
		}
	},
	handleConnect(e) {
		console.log('connect');
		gamepadAPI.addGamepadIfNew(e.gamepad);
	},
	handleDisconnect(e) {
		console.log('disconnect');
		gamepadAPI.removeGamepad(e.gamepad);
	},
	t : String.fromCharCode(0x26AA),
	f : String.fromCharCode(0x26AB),
	onOff(v) {
		return v ? t : f;
	},
	keys : ['index', 'id', 'connected', 'mapping', /*'timestamp'*/],
	processController(info) {
		
		controls.press(info);

		if(settings.debug){
			const {elem, gamepad, axes, buttons} = info;
			const lines = [`gamepad  : ${gamepad.index}`];
			for (const key of gamepadAPI.keys) {
				info[key].textContent = gamepad[key];
			}
			axes.forEach(({axis, value}, ndx) => {
				const off = ndx * 2;
				axis.setAttributeNS(null, 'cx', gamepad.axes[off    ] * gamepadAPI.fudgeFactor);
				axis.setAttributeNS(null, 'cy', gamepad.axes[off + 1] * gamepadAPI.fudgeFactor);
				value.textContent = `${gamepad.axes[off].toFixed(2).padStart(5)},${gamepad.axes[off + 1].toFixed(2).padStart(5)}`;
			});
			buttons.forEach(({circle, value}, ndx) => {
				const button = gamepad.buttons[ndx];
				circle.setAttributeNS(null, 'r', button.value * gamepadAPI.fudgeFactor);
				circle.setAttributeNS(null, 'fill', button.pressed ? 'red' : 'gray');
				value.textContent = `${button.value.toFixed(2)}`;
			});

			/* not sure what this is
			lines.push(`axes     : [${gamepad.axes.map((v, ndx) => `${ndx}: ${v.toFixed(2).padStart(5)}`).join(', ')} ]`);
			lines.push(`buttons  : [${gamepad.buttons.map((v, ndx) => `${ndx}: ${onOff(v.pressed)} ${v.value.toFixed(2)}`).join(', ')} ]`);
			elem.textContent = lines.join('\n');
			*/
		}
	},
	addNewPads() {
		const gamepads = navigator.getGamepads();
		for (let i = 0; i < gamepads.length; i++) {
			const gamepad = gamepads[i]
			if (gamepad) {
				gamepadAPI.addGamepadIfNew(gamepad);
			}
		}
	},
	process() {
		if(settings.debug){
			gamepadAPI.runningElem.textContent = ((performance.now() * 0.001 * 60 | 0) % 100).toString().padStart(2, '0');
		}
		gamepadAPI.addNewPads();  // some browsers add by polling, others by event
		Object.values(gamepadAPI.gamepadsByIndex).forEach(gamepadAPI.processController);
		requestAnimationFrame(gamepadAPI.process);
	}
}
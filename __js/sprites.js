const sprites = {
	spritePlane : document.getElementById('sprites'),
	createPlayerSprite(playerIndex){
		var spriteRaw = document.createElement('div');
		spriteRaw.id = players[playerIndex].id;
		spriteRaw.classList.add('player');
		spriteRaw.dataset.indoorway = false;
		spriteRaw.dataset.playernumber = playerIndex+1;
		spriteRaw.dataset.character = players[playerIndex].name;
		sprites.spritePlane.appendChild(spriteRaw);
		players[playerIndex].sprite = document.getElementById(players[playerIndex].id);
		if(sceneList[game.currentScene].playerStartingPoints){
			scene.moveChars(sceneList[game.currentScene].playerStartingPoints);
		}	
	}
}
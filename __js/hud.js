const hud = {
	createHud(){
		var locationBar = document.createElement('div');
			locationBar.id = 'hud-location';
			locationBar.innerText = sceneList[game.currentScene].label;
		game.hud.appendChild(locationBar);
		var playersBar = document.createElement('div');
			playersBar.id = 'hud-players';
		for(var p=0;p<players.length;p++){
			var playerBox = document.createElement('div');
				playerBox.id = 'hud-player_'+(p+1);
				playerBox.classList.add('hud-player');
				playerBox.dataset.hudcharacter = players[p].name;
			var toprow = document.createElement('div');
				toprow.classList.add('hud-player-toprow');
			var playernumber = document.createElement('div');
				playernumber.classList.add('hud-playernumber');
				playernumber.innerText = 'P'+(p+1);
			var playername = document.createElement('div');
				playername.classList.add('hud-charname');
				playername.innerText = players[p].name;
			var clear = document.createElement('hr');
				clear.classList.add('clear');
			toprow.appendChild(playernumber);
			toprow.appendChild(playername);
			toprow.appendChild(clear);
			playerBox.appendChild(toprow);
			var bottomrow = document.createElement('div');
				bottomrow.classList.add('hud-player-bottomrow');
			var imgWrap = document.createElement('div');
				imgWrap.classList.add('hud-charicon');
			var img = document.createElement('img');
				img.src = players[p].images.base;
			imgWrap.appendChild(img);
			var details = document.createElement('div');
				details.classList.add('hud-chardetails');
			var hpmax = document.createElement('div');
				hpmax.classList.add('hud-hp-max');
			var hp = document.createElement('div');
				hp.classList.add('hud-hp');
				hp.dataset.currenthp = 100;
				hp.style.width = "100%";
			hpmax.appendChild(hp);
			details.appendChild(hpmax);
			var items = document.createElement('div');
				items.classList.add('hud-items');
			details.appendChild(items);
			bottomrow.appendChild(imgWrap);
			bottomrow.appendChild(details);
			bottomrow.appendChild(clear);
			playerBox.appendChild(bottomrow);
			playerBox.appendChild(clear);
			playersBar.appendChild(playerBox);
		}
		game.hud.appendChild(playersBar);
	},
	playersBar : document.getElementById('hud-players'),
	players : document.getElementsByClassName('hud-player'),
	updateHud(panel, value, playerIndex){
		if(panel == 'location'){
			document.getElementById('hud-location').innerText = sceneList[game.currentScene].label;
		}
	}
}
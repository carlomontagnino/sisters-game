## To Do

- [ ] Character Select - Start on first sister
- [ ] Character Select - figure out multiplayer issue `/js/controls.js - line 146`
- [ ] Dialog Boxes - chain multiple boxes.
- [ ] Dialog Boxes - options
- [ ] Does multiplayer work mid-game?
- [ ] START: Pause game
- [ ] B: secondary action... item use, jump, collect item
- [ ] Items: found in boxes (or not). Keys, toys, power ups
- [ ] SELECT: chooses Item?
- [ ] Level : Dream (activated by bed)
- [ ] Level : Walk around in dark
- [ ] Level : side scroll platform game (gymboree?)
- [ ] Games : clean up toys
- [ ] Games : use the potty
- [ ] Games : tag
- [ ] Games: side scroll platformer
- [ ] Games : water all plants
- [ ] Activity : look out window
- [ ] Activity : eat meal
- [ ] Abilities: improved walking/jumping, can pick locks, can hold more items, more HP etc



## Units

This is an idea that is not yet implemented.

1 unit is 32x32 pixels.

- **Sprites:** 1x1.5
- **Doors:** 1x1.5
- **Maps:** 25x18.75
- **Items:** 1x1





# Sisters!

*Sisters!* is a WIP game inspired by my own young daughters. 

I started developing it as a means to introduce them to gaming in a low-stakes environment (they had a hard time with Super Mario Bros... they're 3). I also saw it as an opportunity to learn more about the GamepadAPI.

While the game is _very_ early in development, my plans are for it to include simple puzzles and multiplayer games that the average toddler can figure out (with some guidance). This is not intended to be an educational game, just a game that anyone can enjoy. 

